from rest_framework import serializers

from hikingersApp.models.place import Place
from hikingersApp.models.city import City

from hikingersApp.serializers.CitySerializer import CitySerializer


class PlaceSerializer(serializers.ModelSerializer):

    city = CitySerializer()

    class Meta:
        model = Place
        fields = ['id', 'name', 'description', 'location', 'city']

    def create(self, validated_data):
        cityData = validated_data.pop('City')
        placeInstance = Place.objects.create(**validated_data)
        City.objects.create(city=placeInstance, **cityData)
        return placeInstance

    def to_representation(self, obj):
        place = Place.objects.get(id=obj.id)
        city = City.objects.get(place=obj.id)
        return {
            'id': place.id,
            'name': place.Name,
            'description': place.Description,
            'location': place.Location,
            'city': {
                'id': city.id,
                'name': city.Name,
            }
        }
