from hikingersApp.models.account import Account
from rest_framework import serializers

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['user', 'phoneNumber', 'cellphoneNumber', 'location']
        depth = 1