from .CitySerializer import CitySerializer
from .HotelSerializer import HotelSerializer
from .PackagesSerializer import PackagesSerializer
from .Places_visitedSerializer import Places_visitedSerializer
from .PlaceSerializer import PlaceSerializer
from .UserSerializer import UserSerializer
