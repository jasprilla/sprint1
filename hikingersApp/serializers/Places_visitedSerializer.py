from rest_framework import serializers

from hikingersApp.models.place import Place
from hikingersApp.models.user import User
from hikingersApp.models.places_visited import Places_visited

from hikingersApp.serializers.UserSerializer import UserSerializer
from hikingersApp.serializers.PlaceSerializer import PlaceSerializer


class Places_visitedSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()
    user = UserSerializer()

    class Meta:
        model = Places_visited
        fields = ['id', 'date_visited', 'description',
                  'qualification', 'location', 'user', 'place']

    def create(self, validated_data):
        placeData = validated_data.pop('place')
        userData = validated_data.pop('user')

        placeVisitedInstance = Places_visited.objects.create(**validated_data)

        Place.objects.create(place=placeVisitedInstance, **placeData)
        User.objects.create(user=placeVisitedInstance, **userData)

        return placeVisitedInstance

    def to_representation(self, obj):
        placeVisited = Places_visited.objects.get(id=obj.id)
        user = User.objects.get(placeVisited=obj.id)
        place = Place.objects.get(placeVisited=obj.id)
        return {
            'id': placeVisited.id,
            'date_visited': placeVisited.date_visited,
            'description': placeVisited.Description,
            'qualification': placeVisited.quealification,
            'location': placeVisited.Location,
            'user': {
                'id': user.id,
                'username': user.Username,
                'name': user.Name,
                'email': user.Email,
            },
            'place': {
                'id': place.id,
                'name': place.name,
                'description': place.description,
                'location': place.location,
                'city': place.city,
            }
        }
