from rest_framework import serializers

from hikingersApp.models.place import Place
from hikingersApp.models.package import Package

from hikingersApp.serializers.PlaceSerializer import PlaceSerializer


class PackagesSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()

    class Meta:
        model = Package
        fields = ['id', 'name', 'description', 'values', 'place']

    def create(self, validated_data):
        placeData = validated_data.pop('place')
        PackageInstance = Package.objects.create(**validated_data)
        Place.objects.create(place=PackageInstance, **placeData)
        return PackageInstance

    def to_representation(self, obj):
        package = Package.objects.get(id=obj.id)
        place = Place.objects.get(hotel=obj.id)
        return {
            'id': package.id,
            'name': package.Name,
            'description': package.description,
            'values': package.values,
            'place': {
                'id': place.id,
                'name': place.name,
                'description': place.description,
                'location': place.location,
                'city': place.city,
            }
        }
