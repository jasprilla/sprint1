from rest_framework import serializers

from hikingersApp.models.place import Place
from hikingersApp.models.hotel import Hotel

from hikingersApp.serializers.PlaceSerializer import PlaceSerializer


class HotelSerializer(serializers.ModelSerializer):
    place = PlaceSerializer()

    class Meta:
        model = Hotel
        fields = ['id', 'name', 'location', 'place']

    def create(self, validated_data):
        placeData = validated_data.pop('place')
        HotelInstance = Hotel.objects.create(**validated_data)
        Place.objects.create(place=HotelInstance, **placeData)
        return HotelInstance

    def to_representation(self, obj):
        hotel = Hotel.objects.get(id=obj.id)
        place = Place.objects.get(hotel=obj.id)
        return {
            'id': hotel.id,
            'name': hotel.Name,
            'location': hotel.Location,
            'place': {
                'id': place.id,
                'name': place.name,
                'description': place.description,
                'location': place.location,
                'city': place.city,
            }
        }
