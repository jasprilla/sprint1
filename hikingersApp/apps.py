from django.apps import AppConfig


class HikingersappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hikingersApp'
