from django.utils.translation import gettext_lazy as _
from django.db import models

from hikingersApp.models.user import User

class Account(models.Model):
	
	class Location(models.TextChoices):
		MEDELLIN = 'MED', _('MEDELLIN')
		BOGOTA = 'BOG', _('BOGOTA')
		NOINFORMA = 'NOI', _('NO INFORMA')
		
	id = models.IntegerField(unique=True, null=False, primary_key=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	phoneNumber = models.IntegerField(unique=True, null=False)
	cellphoneNumber = models.IntegerField(unique=True, null=False)
	location = models.CharField(max_length=12, choices=Location.choices, default=Location.NOINFORMA)
