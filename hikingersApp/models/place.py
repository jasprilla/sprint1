from django.db import models
from .city import City


class Place(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('Name', max_length=90)
    description = models.TextField('Description')
    location = models.CharField('Location', max_length=100)
    city = models.ForeignKey(
        City, related_name='identificador_city', on_delete=models.CASCADE)
