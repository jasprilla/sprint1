from django.db import models
from .place import Place


class Package(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('Name', max_length=90)
    description = models.TextField('Description')
    values = models.FloatField()
    place = models.ForeignKey(
        Place, related_name='identificador_place_packages', on_delete=models.CASCADE)
