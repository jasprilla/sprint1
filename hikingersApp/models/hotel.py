from django.db import models
from .place import Place


class Hotel(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('Name', max_length=90)
    location = models.CharField('Location', max_length=100)
    place = models.ForeignKey(
        Place, related_name='identificador_place_hotel', on_delete=models.CASCADE)
