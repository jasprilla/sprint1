from django.db import models
from .place import Place
from .user import User


class Places_visited(models.Model):
    id = models.BigAutoField(primary_key=True)
    date_visited = models.DateTimeField()
    description = models.TextField('Description')
    quealification = models.IntegerField(default=0)
    location = models.CharField('Location', max_length=100)
    user = models.ForeignKey(
        User, related_name='identificador_user', on_delete=models.CASCADE)
    place = models.ForeignKey(
        Place, related_name='identificador_place', on_delete=models.CASCADE)
