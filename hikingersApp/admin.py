from django.contrib import admin
from .models.city import City
from .models.hotel import Hotel
from .models.package import Package
from .models.place import Place
from .models.places_visited import Places_visited
from .models.user import User

# Register your models here.
admin.site.register(City)
admin.site.register(Hotel)
admin.site.register(Package)
admin.site.register(Place)
admin.site.register(Places_visited)
admin.site.register(User)
